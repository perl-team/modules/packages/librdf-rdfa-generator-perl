librdf-rdfa-generator-perl (0.204-2) unstable; urgency=medium

  * Team upload.
  * done_testing-conflict.patch: new: disambiguate done_testing.
    (Closes: #1093341)

 -- Étienne Mollier <emollier@debian.org>  Tue, 21 Jan 2025 20:38:50 +0100

librdf-rdfa-generator-perl (0.204-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.7.0
  * modernize source helper script copyright-check
  * update git-buildpackage config:
    + use DEP-14 branch naming
    + filter-out potential upstream debian dir
    + add usage config
  * update watch file:
    + set dversionmangle=auto
    + add usage config
  * update copyright info:
    + update coverage
    + use field Reference (not License-Reference)

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Aug 2024 19:01:51 +0200

librdf-rdfa-generator-perl (0.200-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Set Testsuite header for perl package.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libattean-perl,
      librdf-ns-curated-perl and liburi-namespacemap-perl.
    + librdf-rdfa-generator-perl: Drop versioned constraint on
      librdf-ns-curated-perl and liburi-namespacemap-perl in Depends.
    + librdf-rdfa-generator-perl: Drop versioned constraint on libattean-perl in
      Recommends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 19:03:38 +0000

librdf-rdfa-generator-perl (0.200-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Jonas Smedegaard ]
  * Simplify rules:
    + Stop resolve build-dependencies in rules file.
    + Use short-form dh sequencer (not cdbs).
      Stop build-depend on cdbs.
  * Wrap and sort control file.
  * Tighten lintian overrides regarding License-Reference.
  * Declare compliance with Debian Policy 4.2.1.
  * Update copyright info:
    + Extend coverage of packaging.
    + Use https protocol in protocol URL.
    + List issue tracker as preferred form of contact.
    + Add alternate source URLs.
    + Extend coverage for main upstream author.
  * Update watch file: Fix typo in usage comment.
  * (Build-)depend on recent librdf-ns-curated-perl.
    Build-depend on and recommend recent libattean-perl
    (not librdf-trine-perl).
    (Build-)depend on recent liburi-namespacemap-perl.
    Recommend librdf-ns-perl.
    Recommend (not build-depend or depend on) librdf-prefixes-perl.
  * Build-depend on libtest-modern-perl.
  * Skip testsuite build-dependencies when bootstrapping.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Nov 2018 02:53:09 +0100

librdf-rdfa-generator-perl (0.106-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release.

  [ Jonas Smedegaard ]
  * Improve short description, based on upstream META hints.
  * Update package relations: Build-depend on libtest-output-perl.
  * Update watch file: Use substitution strings.
  * Modernize cdbs:
    + Resolve archs in maintainer script (not during build).
    + Avoid dh-buildinfo to support building without (fake)root.
    + Stop build-depend on licensecheck dh-buildinfo.
  * Modernize Vcs-* fields:
    + Use git (not cgit) in path.
    + Include .git suffix in path.
  * Declare that regular build targets does not require root access.
  * Declare compliance with Debian Policy 4.1.2.
  * Update copyright info:
    + Stop track no longer shipped file HACKING.pod.
    + Stop track no longer included embedded code copies.
    + Fix strip legacy leading ./ from a file path.
    + Extend coverage for main upstream author(s).
    + Extend coverage for myself.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Dec 2017 13:20:11 +0100

librdf-rdfa-generator-perl (0.103-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Use canonical hostname (anonscm.debian.org) in Vcs-Git URI.
  * Update Vcs-Browser URL to cgit web frontend.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Install examples.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Relicense packaging as GPL-3+.
    + Extend copyrigt coverage for myself to include recent years.
  * Drop patch 1001: Build problem turned out to be (and fixed by now)
    in CDBS.
  * Update watch file:
    + Bump to file format 4.
    + Watch MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field: Use https protocol.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 26 Dec 2016 12:53:55 +0100

librdf-rdfa-generator-perl (0.103-1) unstable; urgency=low

  [ upstream ]
  * New upstream release.

  [ gregor herrmann ]
  * Remove debian/source/local-options: abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

  [ Jonas Smedegaard ]
  * Bump dephelper compatibility level to 8.
  * Bump standards-version to 3.9.4.
  * Update package relations:
    + Stop (build-)depending on libcommon-sense-perl: No longer used
      upstream.
    + Relax to (build-)depend unversioned on cdbs, devscripts and
      debhelper: Needed versions satisfied in stable, and oldstable no
      longer supported.
  * Use anonscm.debian.org for Vcs-Browser field.
  * Add patch 1001 to avoid auto-installing dependencies during build.
  * Update copyright file:
    + Update Files sections for convenience copies of external Perl
      modules.
    + List upstream issue tracker as preferred upstream contact.
    + Fix use pseudo-license and pseudo-comment sections to obey silly
      restrictions of copyright format 1.0.
    + Bump file format to 1.0.
    + Fix strip trailing space.
    + Update copyright file: Add Files section for newly added
      HACKING.pod, and corresponding CC-BY-SA_UK-2.0 License paragraph.
  * Use metacpan.org URL as Homepage.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 13 Dec 2012 14:13:54 +0100

librdf-rdfa-generator-perl (0.102-1) unstable; urgency=low

  * New upstream release.
  * Update copyright file:
    + Fix use separate sections for Artistic and GPL-1+.
    + Improve references for convenience copy of Module::Install.
    + Add references for convenience copy of Test::Signature.
    + Rewrap and shorten comments.
    + Quote licenses in License comments.
  * Drop patch 2001 (disable verification): Upstream no longer verify
    source integrity in testsuite.
  * Bump policy compliance to standards-version 3.9.2.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 07 Jul 2011 10:17:56 +0200

librdf-rdfa-generator-perl (0.101-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#616593.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 05 Mar 2011 23:23:31 +0100
